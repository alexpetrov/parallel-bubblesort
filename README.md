# Parallel sorting algorithm based on bubblesort

compile:
```
mpicc -Wall -O -o bsort bsort.c
```
run:
```
mpirun -np <num_procs> bsort <in_file>
```
