#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

void swap(int *arr, int i, int j){
    int temp = arr[i];
    arr[i] = arr[j];
    arr[j] = temp;
}

void bubblesort(int *arr, int n){
    int i, j;
    for (i = n-2; i >= 0; i--){
        for (j = 0; j <= i; j++){
            if (arr[j] > arr[j+1]){
                swap(arr, j, j+1);
            }
        }
    }
}

int *merge(int *arr1, int *arr2, int length1, int length2){
  int *result = (int*) malloc((length1 + length2) * sizeof(int));
  int i = 0;
  int j = 0;
  int k;
  for (k = 0; k < length1 + length2; k++) {
    if (i >= length1) {
      result[k] = arr2[j];
      j++;
    }
    else if (j >= length2) {
      result[k] = arr1[i];
      i++;
    }
    else if (arr1[i] < arr2[j]) { // indices in bounds as i < n1 && j < n2
      result[k] = arr1[i];
      i++;
    }
    else { // v2[j] <= v1[i]
      result[k] = arr2[j];
      j++;
    }
  }
  return result;
}


int main(int argc, char ** argv)
{
  int n;
  int * data = NULL;
  int c, s;
  int * chunk;
  int o;
  int * other;
  int step;
  int p, id;
  MPI_Status status;
  double total_time;
  FILE * file = NULL;
  int i;

  if (argc != 2) {
    fprintf(stderr, "Usage: mpirun -np <num_procs> %s <in_file>\n", argv[0]);
    exit(1);
  }

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &id);

  if (id == 0) {
    file = fopen(argv[1], "r");
    if(fscanf(file, "%d", &n) == 0){
        printf("There was an error reading the file.");
        exit(1);
    }
    // compute chunk size
    c = n/p; if (n%p) c++;
    // read data from file
    data = (int *)malloc(p*c * sizeof(int));
    for (i = 0; i < n; i++){
      fscanf(file, "%d", &(data[i]));
    }
    fclose(file);
    // pad data with 0 -- doesn't matter
    for (i = n; i < p*c; i++)
      data[i] = 0;
  }

  // start the timer
  MPI_Barrier(MPI_COMM_WORLD);
  total_time = -MPI_Wtime();

  // broadcast size
  MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

  // compute chunk size
  c = n/p; if (n%p) c++;

  // scatter data
  chunk = (int *) malloc(c * sizeof(int));
  MPI_Scatter(data, c, MPI_INT, chunk, c, MPI_INT, 0, MPI_COMM_WORLD);
  free(data);
  data = NULL;

  // compute size of own chunk and sort it
  s = (n >= c * (id+1)) ? c : n - c * id;
  bubblesort(chunk, s);

  // up to log_2 p merge steps
  for (step = 1; step < p; step = 2*step) {
    if (id % (2*step)!=0) {
      // id is no multiple of 2*step: send chunk to id-step and exit loop
      MPI_Send(chunk, s, MPI_INT, id-step, 0, MPI_COMM_WORLD);
      break;
    }
    // id is multiple of 2*step: merge in chunk from id+step (if it exists)
    if (id+step < p) {
      // compute size of chunk to be received
      o = (n >= c * (id+2*step)) ? c * step : n - c * (id+step);
      // receive other chunk
      other = (int *)malloc(o * sizeof(int));
      MPI_Recv(other, o, MPI_INT, id+step, 0, MPI_COMM_WORLD, &status);
      // merge and free memory
      data = merge(chunk, other, s, o);
      free(chunk);
      free(other);
      chunk = data;
      s = s + o;
    }
  }

  // stop the timer
  total_time += MPI_Wtime();

  if (id == 0) {
    printf("Bubblesort on %d items on %d processes: %f s\n", n, p, total_time);
    // printf("%d %2d %f\n", n, p, total_time);
  }

  MPI_Finalize();
  return 0;
}
